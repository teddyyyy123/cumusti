\documentclass[conference]{IEEEtran}
\IEEEoverridecommandlockouts
% The preceding line is only needed to identify funding in the first footnote. If that is unneeded, please comment it out.
\usepackage{cite}
\usepackage{hyperref}

\usepackage{subfigure}
%\usepackage{IEEEtrantools}

\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{xcolor}
\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}
\begin{document}

\title{The Cumulative Stimulus Model to Describe Long Term Behavior
}

\author{\IEEEauthorblockN{1\textsuperscript{st} Yang Yang}
\IEEEauthorblockA{\textit{Facebook.com} \\
yyy@fb.com}
\and
\IEEEauthorblockN{2\textsuperscript{nd} Qichao Que}
\IEEEauthorblockA{\textit{Facebook.com} \\
qichao@fb.com}
\and
\IEEEauthorblockN{3\textsuperscript{nd} Qingyuan Kong}
\IEEEauthorblockA{\textit{Facebook.com} \\
markkong@fb.com}
\and
\IEEEauthorblockN{4\textsuperscript{th} Hsiao-Ping Tseng}
\IEEEauthorblockA{\textit{Facebook.com} \\
hptseng@fb.com}
\and
\IEEEauthorblockN{5\textsuperscript{th} Yi Wang}
\IEEEauthorblockA{\textit{Facebook.com} \\
yiwang1@fb.com}
\and
}

\maketitle

\begin{abstract}
The Cumulative Stimulus Model is a new model arch to describe the causes for
long term user behavior changes. This model can be used for a wide range of
important applications, such as increasing user content production. It is
highly effective, intuitive to understand, and generic, does not rely on any
domain knowledge about individual use cases. It increased user content posts
by 0.48\% in experiments on a test website. Apart from actual ranking, this model provides a
powerful analytical tool to gain insights into the causes for long-term
behavior change.
\end{abstract}

\begin{IEEEkeywords}
producer optimization, Reinforcement Learning, ranking
\end{IEEEkeywords}

\section{Introduction}
We focus on the usecase of an experimental social media website, where
users  generate contents, such as
links, videos, pictures. When a user comes online, he can see a list of 
the latest contents that all his friends has posted. We can rank these contents
 in different orders: oftentimes the goal of ranking is to increase the viewer's engagement (CTR); in this article, we try to optimize the ranking to increase content {\bf production}.


 Such ranking
strategy impacts content production through 2 ways: 1) On the viewer side, by
showing more attractive content to the viewer,  the viewer mimics his friends
and also starts posting. 2) On the producer’s side, for a given producer that
has already posted content, we try to cause more feedbacks to be given to the
producer, so the producer is stimulated by such feedbacks, and he starts
producing more content.

In this article we describe the application of Cumulative Stimulus model in the
use case of producer side effects.

\section{The Mathematical Model for Content Production}

In the following, we gradually illustrate our model, from simple to complete,
filling in details step by step.

\subsection{Effect of User Feedback}

From personal experience, we can see that the more likes and comments we get on
our content, the more likely we are to post more. We can imagine that each
feedback on our content ``pushes'' us towards being more likely to post in the future.
We can quantitatively measure this stimulus effect by a score calculated from a
function on this feedback. How do we calculate this score then? Given that
theoretically, neural network is a universal  function approximator, we just
need to describe this stimulus effect with a neural network, whose features are
everything related to that feedback (including viewer features, producer features,
and viewer-producer cross features).

The tricky part is that stimulus from one feedback generally does not immediately
lead to the content producer starting to post; instead, a content producer gradually
feels more and more tempted to post after he gets more and more feedbacks, until
finally the total stimuli he received crossed a certain threshold. Based on
this intuition, we describe the Cumulative Stimulus from all the feedbacks he got
in the past month, as:
\begin{equation}
 \sum_{i \in \mbox{feedbacks in past month}}S(X_i)
\end{equation}

In the above, $S(X_i)$ is the function that describes the amount of stimulus
resulting from one feedback.

Our model assumes that the Cumulative Stimulus can be matched with the
supervision label, defined as ``whether or not the content producer posts a new
content in the next week''. We train the model by minimizing the cross-entropy
between our Cumulative Stimulus score and the label, but since cross-entropy
requires a score within the [0, 1] range, we further take the sigmoid on the
score,  which gives the probability of producing SOME content in the next week:

\begin{IEEEeqnarray}{rCl}
P(history) & = & Sigmoid(Z) \\
           & = & Sigmoid(\sum_{i \in \mbox{\tiny feedbacks in past month}}S(X_i))\\
 &\Longleftrightarrow & label
\end{IEEEeqnarray}

\subsection{
Native Production Probability
}

The above analysis attributes production probability to feedbacks. But we should
also realize that some portion of the production probability is not due to
feedbacks, i.e. a producer might just keep posting even if he gets no feedbacks at
all. It is important to isolate out this portion of probability, because it’s
not something that we could change through ranking decisions. Hence the above
equation becomes:

\begin{IEEEeqnarray}{rCl}
Sigmoid\left(\sum_{i \in \mbox{\tiny feedbacks in past month}}S(X_i) + N(\tilde{X}) \right)\Longleftrightarrow label
\end{IEEEeqnarray}

Note that in the above, $N(\tilde{X})$ is the neural network calculating the native
production probability for the producer;  $\tilde{X}$ is the producer-only
features at the timestamp, before the month in which the history is collected.
So these features describe the inherent tendency of the producer to post. The following diagram shows
the important timestamp definitions for the features and label, for a given producer:

\begin{figure}[htbp]
\centerline{\includegraphics[width=5cm]{events_series.png}}
\caption{Time Sequence of Events}
\label{fig}
\end{figure}


%\subsection{Effects of Views }
%
%Another small tweak to the model is that, apart from replies, we know that
%views on content would impact story producer future behavior too, because story
%producers are made aware of the views by means of checking the viewer sheet, or
%through notifications. This effect is captured easily by creating a new
%function $S_v$, for the Stimulus from views:
%
%
%\begin{IEEEeqnarray}{rCl}
%Sigmoid\left(\sum_{i \in \mbox{\tiny replies in past month}}S(X_i) + \right. & &\\
%\left. \sum_{j \in \mbox{\tiny views in past month}}S_v(X'_j) + N(\tilde{X})\right)  &\Longleftrightarrow & label\\
%\end{IEEEeqnarray}


\subsection{Time Decay}

Naturally we would believe that feedbacks much earlier in the past would have
less stimulus on the content producer, compared to a feedback received just now. We
account for this effect through a time-decayed weight for each of the 2
stimulus terms:
\begin{IEEEeqnarray}{rCl}
Sigmoid\left(\sum_{i \in \mbox{\tiny feedbacks in past month}}S(X_i)\gamma^{t_i} + \right. & &\\
\left. N(\tilde{X})\right)  &\Longleftrightarrow & label\\
\end{IEEEeqnarray}
where $\gamma$ is a hyper parameter, and $t_i$ is the number of days from the
feedback event to the end of month window.


\subsection{Integration into Production}
At ranking time, the decision we have to make is, if we had one possible feedback,
whom should we give the feedback to, so that the total increase in production is
highest? In  math language, this would be roughly the differential of the
P(history) against history:


\begin{IEEEeqnarray}{rCl}
\frac{dP}{d(history)} & = & \frac{dSigmoid(Z)}{d(history)} \\
&= &\frac{dSigmoid(Z)}{dZ} \times \frac{dZ}{d(history)} \\
& = & P\times (1-P) \times S(X_i)
% \frac{1}{2}
\end{IEEEeqnarray}

where the 3rd term in the last equation above is because the increase in
history causes one extra feedback event, and the extra feedback event causes
increment in Z. We see that we basically want to pick which candidate will get
the highest stimulus from this new feedback. This completes the derivation of our
mathematical model.

Interestingly the P*(1-P) term
modulates the per-event stimulus: P*(1-P) is highest when P=0.5, or Z=0, and
becomes smaller when  P is very small or very close to 1 (see following pic).
Intuitively, this says that when a producer is already very productive, or very
inactive, it's difficult to push him to change his production behavior, while
it's much easier to increase the production behavior of a user "on the
threshold". We will see later in the online results section that it was exactly
due to missing this P*(1-P) term that our earlier experiments failed to yield
expected results.

\begin{figure}[htbp]
\centerline{\includegraphics[width=3cm]{sigmoid.jpeg}}
\caption{the trend of Sigmoid function and its gradient}
\label{fig}
\end{figure}



\section{Model Implementation}


\subsection{Training Data}
For each producer, we collect feedbacks received on his posted content in the past
week, use this to compute the $\sum(S(X_i))$ term; we take his producer features at
the timestamp of one week ago to compute the $N(\tilde{X})$ native term; we take
``whether the user produced at least 1 content in the next week'' as the label.
This gives us one training example.

Then we move to the next day, and repeat the above process, to obtain another
training example. We slide the timestamp for 7 days to get 7 examples. Then for
all producers, we get the entire set of training examples. The sliding window
approach is to avoid training the model to the pattern on a particular day of
week. 

The choice of timestamp to choose for the native term $N(\tilde{x}$ is an
interesting issue. Our original solution actually used the current timestamp,
but then the impact of the stimulus could possibly be already reflected in the
observed producer features (such as ``number of content produced yesterday''), so
the model would attribute the differences in the label mostly to the native
term, which would be useless to us. So by setting the native term timestamp to
before the stimulus starting to take effect, we can attribute the differences
in label more to the stimulus terms, which is what we want.

\section{Model Architecture}

We implemented the aforementioned model using the Caffe2 framework. The network is trained to 
optimize a loss function of cross entropy, and we evaluate model offline performance by normalized entropy (NE),
which is the cross entropy divided by the cross entropy loss if we had given a
uniform score to every example. Functions $S(X_i)$ and $N(\tilde{X})$ are each
represented by a network (to be
trained). Each training example contains the producer's feedback history within
the past week. The history is represented by a list of features. In the overall
network, each $X_i$ passes through the $S$ network, and the results (whose
count is variable for each producer) are summed up by a {\tt LengthSum}
operator. The following diagram shows the network architecture:

\begin{figure}[htbp]
\centerline{\includegraphics[width=5cm]{net_arch.png}}
\caption{Network Architecture of Cumulative Stimulus model}
\label{fig}
\end{figure}



\section{Offline Results}
Two points are particularly interesting from the offline results: 

Feature importance results show the important features agree with our human
understanding of the factors contributing to content production. For example
one feature shows whether the content is a
content reshared from some existing post, it turns out that the definition of
content production, which the
model is trained on, explicitly excludes reshared content from the post metric, so
it makes sense that the model should give low scores to reshared content. The
model learns this fact on its own, without any domain knowledge fed in from
humans.  Another    example is a feature measuring  both mean the closeness between
viewer and  content poster. This corroborates our original hypothesis that
modeling the stimulus at the event level (v.s. at producer level, like previous
approaches) enables us to unearth finer differences from stimulus events.
Content producer country is also shown to be an important feature, implying 
 that probably people from different countries show different
responses to stimulus.                   Additionally, the model shows a lot of
interesting insights, exposing important features which humans did not know
previously, for example: the feature showing the time lag from content posting time to stimulus event
time is shown to be important,  which indicates that
possibly giving a feedback as soon as a content is posted, is going to induce bigger
stimulus for future production.  These conjectures can be
easily verified by dumping the S() score, and plotting the distribution against
these different dimensions. 

Without the stimulus terms $\sum{S(X_i)}$, the model NE does get worse, but only
slightly. This does show that the stimulus terms helps explain the causes for
future production. On the other hand, the label is largely determined by the
native term, which agrees with our previous simple observation that week over
week production metric has 75\% correlation, i.e. production
behavior is very sticky, and slow to change.


\section{Online Results}
Most ranking systems rank candidates by  a combination of various model
scores, which is referred to as a value model. We add $P\times(1-P) \times S(X_i)$ to the existing 
value model. But this term gives the increment in production probability 
{\it given} that a feedback happens on this content. So we have to
multiply this with the {it expected number of feedbacks on this content}, i.e. we need to
add $P(click) * P * (1-P) * S(X_i)$. In practice we also add a fudge factor to the $P(click)$,
so we add to existing VM : $\beta \times (P(click) + \alpha) \times P \times (1-P) \times S(X_i)$.
$\alpha$ and $\beta$ are determined through trials.


%\subsection{Overall Production Metrics Increase}
Overall, increasing the weight $\beta$ clearly leads to higher gains in content production
metric:

\begin{table}[htbp]
\caption{Overall Online Production Metrics Gain}
\begin{center}
\begin{tabular}{|c|c|c|}
\hline
weight $\beta$ & produced content delta & content producers delta \\
\hline
20000 & +0.48\% & +0.4\% \\
\hline
10000 & +0.44\% & +0.36\% \\
\hline
2000 & +0.13\% & +0.11\% \\
\hline
\end{tabular}
\label{tab1}
\end{center}
\end{table}

Interestingly, our model achieved production increases {\it even though} the
overall feedbacks received by all test producers actually dropped by 0.37\%. This means that {\bf we were able to
allocate the stimulus budget to producers in a more efficient way}, i.e. the
main idea of our model.

%\subsection{Views Allocation Breakdown}
%Another interesting aspect from the online results is the breakdown long
%different dimensions, of views received by content. This generally agrees with
%the stimulus score distributions seen in offline results. For example,
%for test producers, reshared posts were given 5\% fewer views. 
%
%If we breakdown the received views changes across producers of different level
%of activeness before the experiment, we can see that level 1 and level 2
%producers are getting the most significant increases in views allocation. This
%is due to the $P \times (1-P)$ term, because these producers current $P$ value
%is about 0.5.
%
%Very interestingly, for a long time, during the previous iterations of this
%model, we did not multiply the P*(1-P) term, and thought such sloppy treatment
%would "sort of work", and indeed it did produce production gains in the initial
%tests, but unfortunately we see the model favor users with higher producer dap.
%This is quite contrary to common intuition, as we would believe that as users
%become already very actively producing, the marginal gain would become lower.
%After sticking to the math exactly and applying the $P\times(1-P)$ term, this was
%immediately fixed:
%
%
%\begin{figure}[htbp] \subfigure[with
%$P\times(1-P)$]{\includegraphics[width=4cm]{pdap.png}}
%\subfigure[without $P\times(1-P)$]{\includegraphics[width=4cm]{pdap_old.png}}
%\caption{production gain vs existing activeness}
%\label{fig_ldap}
%\end{figure}
%
%
%
%\subsection{Production Increase Breakdown}
%Similar to the views breakdown, we see similar trend in production increase breakdown across
%these dimensions: Our media posts increased 0.39% for non-reshared posts, but remain neutral 
%for reshared posts. Across existing level of activeness, we see biggest increase for users with
%level 1 and 2:
%
%\begin{figure}[htbp]
%\centerline{\includegraphics[width=3cm]{pdap_oespd.png}}
%\caption{Producers gain vs activeness}
%\label{fig}
%\end{figure}
%
%Before this work, we tried one rule-based tweaking of scores, favoring certain 
%producers, based on domain knowledge. Tt turns out that 
%our model actually shows the same preference for such producers, without having
%such domain knowledge.
%
%
%finally, another interesting observation from the online results, is that the
%metric increased only slowly,  roughly showing gains after 1 week. This is not
%a coincidence, because it's exactly how the model is trained (to boost
%production after 1 week):
%
%\begin{figure}[htbp]
%\centerline{\includegraphics[width=3cm]{time_series.png}}
%\caption{Producers gain across time}
%\label{fig}
%\end{figure}


\section{Related Work}
The Lift Model~\cite{lift} is often used to study the effect of changing {\bf one} 
factor on some goal metric. The lift model also studies the changes of the goal metric
from one week to the next, and tries to find those users that are most sensitive to
such changes in the tweaked factor. But the lift model normally requires running
a ``seeding experiment'' to collect training data, in which random users are given 
different values for the tweaked factor, as different treatments. Such seeding experiment
makes practical long-term maintenance of the model cumbersome, and limits the amount
of training data available, while these issues do not exist for our model. The other 
limitation of the lift model is that it could only inspect the effect of {\bf one} factor,
such as ``increasing views'', or ``increasing videos views'', or ``increasing cartoon views''.
This makes its application very labor-intensive, where humans would have to
apply domain knowledge to guess which factor to try, from a large group of possible 
factors. Our model simultaneously studies the effects of hundreds of factors, without 
any domain knowledge.

\cite{linkedin_shaping} follows a similar high-level idea as us, in that it tries
to find the delta in goal metric after stimulus. But \cite{linkedin_shaping} essentially
computes the differential {\bf numerically}, and as a result of the noisiness of the
numerical differential, had to apply complex methods to ``smooth out'' the result. Our 
method basically designs the mathematical model into a form that is much more
amenable to differentiation from the very start, and obtains a clean differential
form.

\cite{linkedin_email} uses a similar decomposition into S() and N() terms, but 
it uses a simple logistic regression model, so it is limited to a few fixed 
``email types'', or essentially features; while by using neural network, our model
is able to capture and automatically analyze hundreds of features. \cite{linkedin_email}
also does not address the $P\times(1-P)$ issue.

Finally, our method bears some similarity to Reinforcement Learning. RL is indeed the
golden standard in modeling long term strategies (chess, Go...).  Indeed many
aspects of our method are inspired by RL, for example the time decay, and
adding up of stimulus, which is similar to the add up of rewards. In fact, we
pretty much remove considerations about state transitions in RL (or you can
consider that we only have 1 state).

One difficult part of applying RL to ranking is that almost all ranking systems
finally rank by a very arbitrary VM, which is a combination of terms, and
it's very difficult to say by exactly what probability a candidate will appear.
But both the on-policy and off-policy (DDPG) variants of RL require such a
probability to be known.





\begin{thebibliography}{00}
\bibitem{b1} The Caffe2 deep learning framework {\href{https://caffe2.ai/}{https://caffe2.ai/ }}.
\bibitem{linkedin_shaping} Ye Tu , Chun Lo , Yiping Yuan , Shaunak Chatterjee.
Feedback Shaping: A Modeling Approach to Nurture Content Creation.
 KDD '19: Proceedings of the 25th ACM SIGKDD International Conference on Knowledge Discovery \& Data MiningJuly 2019 Pages 2241–2250.
\bibitem{linkedin_email}Rupesh Gupta , Guanfeng Liang , Romer Rosales.
Optimizing Email Volume For Sitewide Engagement.
 CIKM '17: Proceedings of the 2017 ACM on Conference on Information and Knowledge Management. November 2017 Pages 1947–1955
\bibitem{lift} P. Rzepakowski and S. Jaroszewicz, ``Decision trees for uplift modeling with single and multiple treatments''. Knowledge and Information Systems v32 , 303–327 (2012)

\end{thebibliography}
\end{document}
